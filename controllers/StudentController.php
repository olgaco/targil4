<?php

namespace app\controllers;

use Yii;

use yii\web\Controller;
use app\models\Student;


class StudentController extends Controller
{
   
    public function actionView($id)
    {
		//echo "Student controller works";
		$name = Student::getName($id); // קריאה לפונקציה סטטית
		$IDnumber = Student::getIDnumber($id);
		$age = Student::getAge($id);
        return $this->render('view',['name' => $name ,'IDnumber' => $IDnumber ,'age'=> $age]); // הפונקציה render אחראית על העברת המידע ל view
    }
}
