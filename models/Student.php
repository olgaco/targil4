<?php

namespace app\models;
use yii\db\ActiveRecord;

class Student extends ActiveRecord 
{
/*
	public static function tableName()
	{
		return 'student';
	}

    private static $students = [
        '1' => [
            'name' => 'Jack',

        ],
        '2' => [
            
            'name' => 'John',

        ],
    ];
*/

		public static function getName($id)
		{
			$student = self::findOne($id);
			
				isset($student)? // בדיקה לוגית
				$return= $student->name: // הערך הראשון ל true
				$return ="No Student found with id $id"; // הערך השני ל false
				return $return;
		}
		public static function getIDnumber($id)
		{
			$student = self::findOne($id);
			
				isset($student)? // בדיקה לוגית
				$return= $student->IDnumber: // הערך הראשון ל true
				$return ="No Student found with id $id"; // הערך השני ל false
				return $return;
		}
		public static function getAge($id)
		{
			$student = self::findOne($id);
			
				isset($student)? // בדיקה לוגית
				$return= $student->age: // הערך הראשון ל true
				$return ="No Student found with id $id"; // הערך השני ל false
				return $return;
		}
}

